# 实全网络调试助手

#### 介绍
    网络调试助手是集TCP/UDP服务端客户端一体的网络调试工具，可以帮助网络应用设计、开发、测试人员检查所开发的网络应用软硬件的数据收发状况，提高开发的速度，是TCP/UDP应用开发助手。
   实全网络调试助手是基于C# .net Socket 网络调试工具。
![实全网络调试助手](https://images.gitee.com/uploads/images/2021/0725/204651_c932abd5_1063145.png "实全网络调试助手.png")

TCP 服务端
![输入图片说明](https://images.gitee.com/uploads/images/2021/0725/204819_58317d74_1063145.png "TCPServer.png")

TCP 客户端
![输入图片说明](https://images.gitee.com/uploads/images/2021/0725/204841_2f14d799_1063145.png "TCPClient.png")

#### 软件架构
软件架构说明


#### 安装教程
绿色软件，解压运行

#### 使用说明


#### 参与贡献


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
