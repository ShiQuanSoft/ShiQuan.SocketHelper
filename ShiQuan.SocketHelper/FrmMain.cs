﻿
using ShiQuan.Framework;
using ShiQuan.IOCPSocket;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ShiQuan.SocketHelper
{
    public class FrmMain : Form
    {
        private ToolStrip sysToolBar;
        private StatusStrip sysStatusBar;
        private Panel panel1;
        private GroupBox groupBox1;
        private Button btnConnect;
        private ToolStripStatusLabel sysStatusLabel1;
        private Panel panel2;
        private Panel panel3;
        private Button btnSend;
        private Label labPort;
        private TextBox txtIPString;
        private Label labIPString;
        private ComboBox cbxProtocolType;
        private Label label1;
        private RichTextBox txtSendContent;
        private GroupBox groupBox3;
        private CheckBox cbxLoopSend;
        private Label label8;
        private TextBox txtLoopInterval;
        private Label label7;
        private LinkLabel btnClear;
        private LinkLabel btnSelect;
        private Label label6;
        private TextBox txtPort;
        private GroupBox gbxResult;
        private RichTextBox txtResult;
        private Panel panelTcpClient;
        private Panel panelUDPSocket;
        private Panel panelTcpServer;
        private Splitter splitter1;
        private NotifyIcon sysNotifyIcon;
        private ContextMenuStrip sysContextMenu;
        private ToolStripMenuItem mnuShow;
        private ToolStripMenuItem mnuExit;
        private ComboBox cbxConntects;
        private Label label2;
        private TextBox txtRemotePort;
        private Label label9;
        private ComboBox txtRemoteIPString;
        private Label label3;
        private Label label10;
        private ComboBox cbxSendEncoding;
        private GroupBox groupBox4;
        private ComboBox cbxReceiveEncoding;
        private CheckBox ckbAppendTime;
        private Label label12;
        private ToolStripDropDownButton tbbHelper;
        private ToolStripMenuItem mnuAbout;
        private ToolStripSeparator toolStripMenuItem1;
        private CheckBox ckbAppendN;
        private ToolStripButton tbbSave;
        private ToolStripButton tbbClear;
        private TextBox txtLocalIpString;
        private CheckBox ckbAppendAddress;
        private ToolStripStatusLabel sysStatusLabel2;
        private ToolStripMenuItem mnuCheckUpdate;

        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmMain));
            this.sysToolBar = new System.Windows.Forms.ToolStrip();
            this.tbbHelper = new System.Windows.Forms.ToolStripDropDownButton();
            this.mnuAbout = new System.Windows.Forms.ToolStripMenuItem();
            this.tbbSave = new System.Windows.Forms.ToolStripButton();
            this.tbbClear = new System.Windows.Forms.ToolStripButton();
            this.sysStatusBar = new System.Windows.Forms.StatusStrip();
            this.sysStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.sysStatusLabel2 = new System.Windows.Forms.ToolStripStatusLabel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.ckbAppendAddress = new System.Windows.Forms.CheckBox();
            this.ckbAppendN = new System.Windows.Forms.CheckBox();
            this.cbxReceiveEncoding = new System.Windows.Forms.ComboBox();
            this.ckbAppendTime = new System.Windows.Forms.CheckBox();
            this.label12 = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.cbxSendEncoding = new System.Windows.Forms.ComboBox();
            this.cbxLoopSend = new System.Windows.Forms.CheckBox();
            this.label8 = new System.Windows.Forms.Label();
            this.txtLoopInterval = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.btnClear = new System.Windows.Forms.LinkLabel();
            this.btnSelect = new System.Windows.Forms.LinkLabel();
            this.label6 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.txtPort = new System.Windows.Forms.TextBox();
            this.labPort = new System.Windows.Forms.Label();
            this.txtIPString = new System.Windows.Forms.TextBox();
            this.labIPString = new System.Windows.Forms.Label();
            this.cbxProtocolType = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.btnConnect = new System.Windows.Forms.Button();
            this.panel2 = new System.Windows.Forms.Panel();
            this.gbxResult = new System.Windows.Forms.GroupBox();
            this.txtResult = new System.Windows.Forms.RichTextBox();
            this.panelTcpClient = new System.Windows.Forms.Panel();
            this.txtLocalIpString = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.panelUDPSocket = new System.Windows.Forms.Panel();
            this.txtRemotePort = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.txtRemoteIPString = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.panelTcpServer = new System.Windows.Forms.Panel();
            this.cbxConntects = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.splitter1 = new System.Windows.Forms.Splitter();
            this.panel3 = new System.Windows.Forms.Panel();
            this.txtSendContent = new System.Windows.Forms.RichTextBox();
            this.btnSend = new System.Windows.Forms.Button();
            this.sysNotifyIcon = new System.Windows.Forms.NotifyIcon(this.components);
            this.sysContextMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.mnuShow = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripSeparator();
            this.mnuExit = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuCheckUpdate = new System.Windows.Forms.ToolStripMenuItem();
            this.sysToolBar.SuspendLayout();
            this.sysStatusBar.SuspendLayout();
            this.panel1.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.gbxResult.SuspendLayout();
            this.panelTcpClient.SuspendLayout();
            this.panelUDPSocket.SuspendLayout();
            this.panelTcpServer.SuspendLayout();
            this.panel3.SuspendLayout();
            this.sysContextMenu.SuspendLayout();
            this.SuspendLayout();
            // 
            // sysToolBar
            // 
            this.sysToolBar.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tbbHelper,
            this.tbbSave,
            this.tbbClear});
            this.sysToolBar.Location = new System.Drawing.Point(0, 0);
            this.sysToolBar.Name = "sysToolBar";
            this.sysToolBar.Size = new System.Drawing.Size(784, 25);
            this.sysToolBar.TabIndex = 3;
            this.sysToolBar.Text = "toolStrip1";
            // 
            // tbbHelper
            // 
            this.tbbHelper.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.tbbHelper.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mnuCheckUpdate,
            this.mnuAbout});
            this.tbbHelper.Image = global::ShiQuan.SocketHelper.Properties.Resources.help;
            this.tbbHelper.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tbbHelper.Name = "tbbHelper";
            this.tbbHelper.Size = new System.Drawing.Size(61, 22);
            this.tbbHelper.Text = "帮助";
            // 
            // mnuAbout
            // 
            this.mnuAbout.Name = "mnuAbout";
            this.mnuAbout.Size = new System.Drawing.Size(152, 22);
            this.mnuAbout.Text = "关于";
            // 
            // tbbSave
            // 
            this.tbbSave.Image = global::ShiQuan.SocketHelper.Properties.Resources.page_save;
            this.tbbSave.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tbbSave.Name = "tbbSave";
            this.tbbSave.Size = new System.Drawing.Size(76, 22);
            this.tbbSave.Text = "保存数据";
            // 
            // tbbClear
            // 
            this.tbbClear.Image = global::ShiQuan.SocketHelper.Properties.Resources.page_delete;
            this.tbbClear.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tbbClear.Name = "tbbClear";
            this.tbbClear.Size = new System.Drawing.Size(76, 22);
            this.tbbClear.Text = "清除显示";
            // 
            // sysStatusBar
            // 
            this.sysStatusBar.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.sysStatusLabel1,
            this.sysStatusLabel2});
            this.sysStatusBar.Location = new System.Drawing.Point(0, 535);
            this.sysStatusBar.Name = "sysStatusBar";
            this.sysStatusBar.Size = new System.Drawing.Size(784, 26);
            this.sysStatusBar.TabIndex = 5;
            // 
            // sysStatusLabel1
            // 
            this.sysStatusLabel1.BorderSides = ((System.Windows.Forms.ToolStripStatusLabelBorderSides)((System.Windows.Forms.ToolStripStatusLabelBorderSides.Left | System.Windows.Forms.ToolStripStatusLabelBorderSides.Right)));
            this.sysStatusLabel1.Name = "sysStatusLabel1";
            this.sysStatusLabel1.Size = new System.Drawing.Size(384, 21);
            this.sysStatusLabel1.Spring = true;
            this.sysStatusLabel1.Text = "技术支持：实全软件科技有限公司 QQ-896374871";
            this.sysStatusLabel1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // sysStatusLabel2
            // 
            this.sysStatusLabel2.BorderSides = ((System.Windows.Forms.ToolStripStatusLabelBorderSides)((System.Windows.Forms.ToolStripStatusLabelBorderSides.Left | System.Windows.Forms.ToolStripStatusLabelBorderSides.Right)));
            this.sysStatusLabel2.Name = "sysStatusLabel2";
            this.sysStatusLabel2.Size = new System.Drawing.Size(384, 21);
            this.sysStatusLabel2.Spring = true;
            this.sysStatusLabel2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.groupBox4);
            this.panel1.Controls.Add(this.groupBox3);
            this.panel1.Controls.Add(this.groupBox1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel1.Location = new System.Drawing.Point(0, 25);
            this.panel1.Name = "panel1";
            this.panel1.Padding = new System.Windows.Forms.Padding(5, 2, 5, 5);
            this.panel1.Size = new System.Drawing.Size(200, 510);
            this.panel1.TabIndex = 6;
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.ckbAppendAddress);
            this.groupBox4.Controls.Add(this.ckbAppendN);
            this.groupBox4.Controls.Add(this.cbxReceiveEncoding);
            this.groupBox4.Controls.Add(this.ckbAppendTime);
            this.groupBox4.Controls.Add(this.label12);
            this.groupBox4.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox4.Location = new System.Drawing.Point(5, 195);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Padding = new System.Windows.Forms.Padding(5);
            this.groupBox4.Size = new System.Drawing.Size(190, 114);
            this.groupBox4.TabIndex = 3;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "接收区设置";
            // 
            // ckbAppendAddress
            // 
            this.ckbAppendAddress.AutoSize = true;
            this.ckbAppendAddress.Checked = true;
            this.ckbAppendAddress.CheckState = System.Windows.Forms.CheckState.Checked;
            this.ckbAppendAddress.Location = new System.Drawing.Point(11, 42);
            this.ckbAppendAddress.Name = "ckbAppendAddress";
            this.ckbAppendAddress.Size = new System.Drawing.Size(120, 16);
            this.ckbAppendAddress.TabIndex = 18;
            this.ckbAppendAddress.Text = "自动附加远程地址";
            this.ckbAppendAddress.UseVisualStyleBackColor = true;
            // 
            // ckbAppendN
            // 
            this.ckbAppendN.AutoSize = true;
            this.ckbAppendN.Checked = true;
            this.ckbAppendN.CheckState = System.Windows.Forms.CheckState.Checked;
            this.ckbAppendN.Location = new System.Drawing.Point(11, 86);
            this.ckbAppendN.Name = "ckbAppendN";
            this.ckbAppendN.Size = new System.Drawing.Size(132, 16);
            this.ckbAppendN.TabIndex = 17;
            this.ckbAppendN.Text = "自动附加换行分隔符";
            this.ckbAppendN.UseVisualStyleBackColor = true;
            // 
            // cbxReceiveEncoding
            // 
            this.cbxReceiveEncoding.FormattingEnabled = true;
            this.cbxReceiveEncoding.Location = new System.Drawing.Point(68, 16);
            this.cbxReceiveEncoding.Name = "cbxReceiveEncoding";
            this.cbxReceiveEncoding.Size = new System.Drawing.Size(114, 20);
            this.cbxReceiveEncoding.TabIndex = 16;
            // 
            // ckbAppendTime
            // 
            this.ckbAppendTime.AutoSize = true;
            this.ckbAppendTime.Checked = true;
            this.ckbAppendTime.CheckState = System.Windows.Forms.CheckState.Checked;
            this.ckbAppendTime.Location = new System.Drawing.Point(11, 64);
            this.ckbAppendTime.Name = "ckbAppendTime";
            this.ckbAppendTime.Size = new System.Drawing.Size(120, 16);
            this.ckbAppendTime.TabIndex = 11;
            this.ckbAppendTime.Text = "自动附加接收时间";
            this.ckbAppendTime.UseVisualStyleBackColor = true;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(9, 19);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(53, 12);
            this.label12.TabIndex = 3;
            this.label12.Text = "编码格式";
            this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.cbxSendEncoding);
            this.groupBox3.Controls.Add(this.cbxLoopSend);
            this.groupBox3.Controls.Add(this.label8);
            this.groupBox3.Controls.Add(this.txtLoopInterval);
            this.groupBox3.Controls.Add(this.label7);
            this.groupBox3.Controls.Add(this.btnClear);
            this.groupBox3.Controls.Add(this.btnSelect);
            this.groupBox3.Controls.Add(this.label6);
            this.groupBox3.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.groupBox3.Location = new System.Drawing.Point(5, 392);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Padding = new System.Windows.Forms.Padding(5);
            this.groupBox3.Size = new System.Drawing.Size(190, 113);
            this.groupBox3.TabIndex = 2;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "发送区设置";
            // 
            // cbxSendEncoding
            // 
            this.cbxSendEncoding.FormattingEnabled = true;
            this.cbxSendEncoding.Location = new System.Drawing.Point(66, 16);
            this.cbxSendEncoding.Name = "cbxSendEncoding";
            this.cbxSendEncoding.Size = new System.Drawing.Size(116, 20);
            this.cbxSendEncoding.TabIndex = 16;
            // 
            // cbxLoopSend
            // 
            this.cbxLoopSend.AutoSize = true;
            this.cbxLoopSend.Location = new System.Drawing.Point(9, 42);
            this.cbxLoopSend.Name = "cbxLoopSend";
            this.cbxLoopSend.Size = new System.Drawing.Size(108, 16);
            this.cbxLoopSend.TabIndex = 15;
            this.cbxLoopSend.Text = "数据流循环发送";
            this.cbxLoopSend.UseVisualStyleBackColor = true;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(154, 64);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(29, 12);
            this.label8.TabIndex = 14;
            this.label8.Text = "毫秒";
            // 
            // txtLoopInterval
            // 
            this.txtLoopInterval.Location = new System.Drawing.Point(68, 61);
            this.txtLoopInterval.Name = "txtLoopInterval";
            this.txtLoopInterval.Size = new System.Drawing.Size(83, 21);
            this.txtLoopInterval.TabIndex = 13;
            this.txtLoopInterval.Text = "1000";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(8, 64);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(53, 12);
            this.label7.TabIndex = 12;
            this.label7.Text = "发送间隔";
            // 
            // btnClear
            // 
            this.btnClear.AutoSize = true;
            this.btnClear.Location = new System.Drawing.Point(98, 91);
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(53, 12);
            this.btnClear.TabIndex = 10;
            this.btnClear.TabStop = true;
            this.btnClear.Text = "清除输入";
            // 
            // btnSelect
            // 
            this.btnSelect.AutoSize = true;
            this.btnSelect.Location = new System.Drawing.Point(8, 91);
            this.btnSelect.Name = "btnSelect";
            this.btnSelect.Size = new System.Drawing.Size(53, 12);
            this.btnSelect.TabIndex = 9;
            this.btnSelect.TabStop = true;
            this.btnSelect.Text = "文件载入";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(7, 19);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(53, 12);
            this.label6.TabIndex = 3;
            this.label6.Text = "编码格式";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.txtPort);
            this.groupBox1.Controls.Add(this.labPort);
            this.groupBox1.Controls.Add(this.txtIPString);
            this.groupBox1.Controls.Add(this.labIPString);
            this.groupBox1.Controls.Add(this.cbxProtocolType);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.btnConnect);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox1.Location = new System.Drawing.Point(5, 2);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(190, 193);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "网络设置";
            // 
            // txtPort
            // 
            this.txtPort.Dock = System.Windows.Forms.DockStyle.Top;
            this.txtPort.Location = new System.Drawing.Point(3, 127);
            this.txtPort.Name = "txtPort";
            this.txtPort.Size = new System.Drawing.Size(184, 21);
            this.txtPort.TabIndex = 8;
            this.txtPort.Text = "18099";
            // 
            // labPort
            // 
            this.labPort.Dock = System.Windows.Forms.DockStyle.Top;
            this.labPort.Location = new System.Drawing.Point(3, 104);
            this.labPort.Name = "labPort";
            this.labPort.Size = new System.Drawing.Size(184, 23);
            this.labPort.TabIndex = 7;
            this.labPort.Text = "（2）服务器商端口";
            this.labPort.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtIPString
            // 
            this.txtIPString.Dock = System.Windows.Forms.DockStyle.Top;
            this.txtIPString.Location = new System.Drawing.Point(3, 83);
            this.txtIPString.Name = "txtIPString";
            this.txtIPString.Size = new System.Drawing.Size(184, 21);
            this.txtIPString.TabIndex = 6;
            this.txtIPString.Text = "127.0.0.1";
            // 
            // labIPString
            // 
            this.labIPString.Dock = System.Windows.Forms.DockStyle.Top;
            this.labIPString.Location = new System.Drawing.Point(3, 60);
            this.labIPString.Name = "labIPString";
            this.labIPString.Size = new System.Drawing.Size(184, 23);
            this.labIPString.TabIndex = 5;
            this.labIPString.Text = "（2）服务器IP地址";
            this.labIPString.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // cbxProtocolType
            // 
            this.cbxProtocolType.Dock = System.Windows.Forms.DockStyle.Top;
            this.cbxProtocolType.FormattingEnabled = true;
            this.cbxProtocolType.Location = new System.Drawing.Point(3, 40);
            this.cbxProtocolType.Name = "cbxProtocolType";
            this.cbxProtocolType.Size = new System.Drawing.Size(184, 20);
            this.cbxProtocolType.TabIndex = 4;
            // 
            // label1
            // 
            this.label1.Dock = System.Windows.Forms.DockStyle.Top;
            this.label1.Location = new System.Drawing.Point(3, 17);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(184, 23);
            this.label1.TabIndex = 3;
            this.label1.Text = "（1）协议类型";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnConnect
            // 
            this.btnConnect.Location = new System.Drawing.Point(30, 154);
            this.btnConnect.Name = "btnConnect";
            this.btnConnect.Size = new System.Drawing.Size(120, 32);
            this.btnConnect.TabIndex = 0;
            this.btnConnect.Text = "连接";
            this.btnConnect.UseVisualStyleBackColor = true;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.gbxResult);
            this.panel2.Controls.Add(this.splitter1);
            this.panel2.Controls.Add(this.panel3);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(200, 25);
            this.panel2.Name = "panel2";
            this.panel2.Padding = new System.Windows.Forms.Padding(5, 2, 5, 0);
            this.panel2.Size = new System.Drawing.Size(584, 510);
            this.panel2.TabIndex = 7;
            // 
            // gbxResult
            // 
            this.gbxResult.Controls.Add(this.txtResult);
            this.gbxResult.Controls.Add(this.panelTcpClient);
            this.gbxResult.Controls.Add(this.panelUDPSocket);
            this.gbxResult.Controls.Add(this.panelTcpServer);
            this.gbxResult.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gbxResult.Location = new System.Drawing.Point(5, 2);
            this.gbxResult.Name = "gbxResult";
            this.gbxResult.Size = new System.Drawing.Size(574, 440);
            this.gbxResult.TabIndex = 11;
            this.gbxResult.TabStop = false;
            this.gbxResult.Text = "网络数据接收";
            // 
            // txtResult
            // 
            this.txtResult.BackColor = System.Drawing.SystemColors.Info;
            this.txtResult.Location = new System.Drawing.Point(3, 17);
            this.txtResult.Name = "txtResult";
            this.txtResult.ReadOnly = true;
            this.txtResult.Size = new System.Drawing.Size(568, 176);
            this.txtResult.TabIndex = 7;
            this.txtResult.Text = "";
            // 
            // panelTcpClient
            // 
            this.panelTcpClient.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panelTcpClient.Controls.Add(this.txtLocalIpString);
            this.panelTcpClient.Controls.Add(this.label10);
            this.panelTcpClient.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelTcpClient.Location = new System.Drawing.Point(3, 323);
            this.panelTcpClient.Name = "panelTcpClient";
            this.panelTcpClient.Size = new System.Drawing.Size(568, 38);
            this.panelTcpClient.TabIndex = 6;
            this.panelTcpClient.Visible = false;
            // 
            // txtLocalIpString
            // 
            this.txtLocalIpString.BackColor = System.Drawing.SystemColors.Info;
            this.txtLocalIpString.Location = new System.Drawing.Point(74, 10);
            this.txtLocalIpString.Name = "txtLocalIpString";
            this.txtLocalIpString.ReadOnly = true;
            this.txtLocalIpString.Size = new System.Drawing.Size(150, 21);
            this.txtLocalIpString.TabIndex = 4;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(3, 13);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(65, 12);
            this.label10.TabIndex = 2;
            this.label10.Text = "本地主机：";
            // 
            // panelUDPSocket
            // 
            this.panelUDPSocket.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panelUDPSocket.Controls.Add(this.txtRemotePort);
            this.panelUDPSocket.Controls.Add(this.label9);
            this.panelUDPSocket.Controls.Add(this.txtRemoteIPString);
            this.panelUDPSocket.Controls.Add(this.label3);
            this.panelUDPSocket.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelUDPSocket.Location = new System.Drawing.Point(3, 361);
            this.panelUDPSocket.Name = "panelUDPSocket";
            this.panelUDPSocket.Size = new System.Drawing.Size(568, 38);
            this.panelUDPSocket.TabIndex = 5;
            this.panelUDPSocket.Visible = false;
            // 
            // txtRemotePort
            // 
            this.txtRemotePort.Location = new System.Drawing.Point(301, 8);
            this.txtRemotePort.Name = "txtRemotePort";
            this.txtRemotePort.Size = new System.Drawing.Size(83, 21);
            this.txtRemotePort.TabIndex = 14;
            this.txtRemotePort.Text = "8088";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(230, 11);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(65, 12);
            this.label9.TabIndex = 3;
            this.label9.Text = "目标端口：";
            // 
            // txtRemoteIPString
            // 
            this.txtRemoteIPString.FormattingEnabled = true;
            this.txtRemoteIPString.Location = new System.Drawing.Point(74, 8);
            this.txtRemoteIPString.Name = "txtRemoteIPString";
            this.txtRemoteIPString.Size = new System.Drawing.Size(150, 20);
            this.txtRemoteIPString.TabIndex = 2;
            this.txtRemoteIPString.Text = "127.0.0.1";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(3, 11);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(65, 12);
            this.label3.TabIndex = 1;
            this.label3.Text = "目标主机：";
            // 
            // panelTcpServer
            // 
            this.panelTcpServer.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panelTcpServer.Controls.Add(this.cbxConntects);
            this.panelTcpServer.Controls.Add(this.label2);
            this.panelTcpServer.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelTcpServer.Location = new System.Drawing.Point(3, 399);
            this.panelTcpServer.Name = "panelTcpServer";
            this.panelTcpServer.Size = new System.Drawing.Size(568, 38);
            this.panelTcpServer.TabIndex = 4;
            this.panelTcpServer.Visible = false;
            // 
            // cbxConntects
            // 
            this.cbxConntects.FormattingEnabled = true;
            this.cbxConntects.Location = new System.Drawing.Point(74, 9);
            this.cbxConntects.Name = "cbxConntects";
            this.cbxConntects.Size = new System.Drawing.Size(150, 20);
            this.cbxConntects.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(3, 12);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(65, 12);
            this.label2.TabIndex = 0;
            this.label2.Text = "连接对象：";
            // 
            // splitter1
            // 
            this.splitter1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.splitter1.Location = new System.Drawing.Point(5, 442);
            this.splitter1.Name = "splitter1";
            this.splitter1.Size = new System.Drawing.Size(574, 5);
            this.splitter1.TabIndex = 10;
            this.splitter1.TabStop = false;
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.txtSendContent);
            this.panel3.Controls.Add(this.btnSend);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel3.Location = new System.Drawing.Point(5, 447);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(574, 63);
            this.panel3.TabIndex = 0;
            // 
            // txtSendContent
            // 
            this.txtSendContent.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtSendContent.Location = new System.Drawing.Point(0, 0);
            this.txtSendContent.Name = "txtSendContent";
            this.txtSendContent.Size = new System.Drawing.Size(499, 63);
            this.txtSendContent.TabIndex = 2;
            this.txtSendContent.Text = "实全软件 QQ:896374871";
            // 
            // btnSend
            // 
            this.btnSend.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnSend.Location = new System.Drawing.Point(499, 0);
            this.btnSend.Name = "btnSend";
            this.btnSend.Size = new System.Drawing.Size(75, 63);
            this.btnSend.TabIndex = 1;
            this.btnSend.Text = "发送";
            this.btnSend.UseVisualStyleBackColor = true;
            // 
            // sysNotifyIcon
            // 
            this.sysNotifyIcon.ContextMenuStrip = this.sysContextMenu;
            this.sysNotifyIcon.Icon = ((System.Drawing.Icon)(resources.GetObject("sysNotifyIcon.Icon")));
            this.sysNotifyIcon.Text = "notifyIcon1";
            this.sysNotifyIcon.Visible = true;
            // 
            // sysContextMenu
            // 
            this.sysContextMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mnuShow,
            this.toolStripMenuItem1,
            this.mnuExit});
            this.sysContextMenu.Name = "sysContextMenu";
            this.sysContextMenu.Size = new System.Drawing.Size(101, 54);
            // 
            // mnuShow
            // 
            this.mnuShow.Name = "mnuShow";
            this.mnuShow.Size = new System.Drawing.Size(100, 22);
            this.mnuShow.Text = "显示";
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(97, 6);
            // 
            // mnuExit
            // 
            this.mnuExit.Name = "mnuExit";
            this.mnuExit.Size = new System.Drawing.Size(100, 22);
            this.mnuExit.Text = "退出";
            // 
            // mnuCheckUpdate
            // 
            this.mnuCheckUpdate.Name = "mnuCheckUpdate";
            this.mnuCheckUpdate.Size = new System.Drawing.Size(152, 22);
            this.mnuCheckUpdate.Text = "检查更新";
            // 
            // FrmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(784, 561);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.sysStatusBar);
            this.Controls.Add(this.sysToolBar);
            this.Name = "FrmMain";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Form1";
            this.sysToolBar.ResumeLayout(false);
            this.sysToolBar.PerformLayout();
            this.sysStatusBar.ResumeLayout(false);
            this.sysStatusBar.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.gbxResult.ResumeLayout(false);
            this.panelTcpClient.ResumeLayout(false);
            this.panelTcpClient.PerformLayout();
            this.panelUDPSocket.ResumeLayout(false);
            this.panelUDPSocket.PerformLayout();
            this.panelTcpServer.ResumeLayout(false);
            this.panelTcpServer.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.sysContextMenu.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        public FrmMain()
        {
            InitializeComponent();

            this.Load += FrmMain_Load;
        }
        UDPSocket _UDPSocket = null;
        TcpSocketServer _TcpServer = null;
        TcpSocketClient _TcpClient = null;
        private void FrmMain_Load(object sender, EventArgs e)
        {
            this.Text = this.sysNotifyIcon.Text = Application.ProductName + "(" + Application.ProductVersion + ")";

            this.btnConnect.Click += BtnConnect_Click;
            this.btnSend.Click += BtnSend_Click;
            this.tbbClear.Click += TbbClear_Click;
            this.tbbSave.Click += TbbSave_Click;
            this.mnuAbout.Click += MnuAbout_Click;
            this.btnClear.Click += BtnClear_Click;
            this.btnSelect.Click += BtnSelect_Click;
            this.mnuShow.Click += MnuShow_Click;
            this.sysNotifyIcon.DoubleClick += SysNotifyIcon_DoubleClick;
            this.FormClosing += FrmMain_FormClosing;
            this.mnuCheckUpdate.Click += MnuCheckUpdate_Click;
            

            this.cbxProtocolType.Items.Clear();
            this.cbxProtocolType.Items.Add("UDP");
            this.cbxProtocolType.Items.Add("TCP Client");
            this.cbxProtocolType.Items.Add("TCP Server");
            this.cbxProtocolType.DropDownStyle = ComboBoxStyle.DropDownList;
            this.cbxProtocolType.SelectedIndexChanged += CbxSocketType_SelectedIndexChanged;
            this.cbxProtocolType.Text = "UDP";

            this.cbxReceiveEncoding.Items.Clear();
            this.cbxReceiveEncoding.Items.Add(System.Text.Encoding.Default.BodyName.ToUpper());
            this.cbxReceiveEncoding.Items.Add(System.Text.Encoding.UTF8.BodyName.ToUpper());
            this.cbxReceiveEncoding.Items.Add(System.Text.Encoding.ASCII.BodyName.ToUpper());
            this.cbxReceiveEncoding.Items.Add("16进制");
            this.cbxReceiveEncoding.DropDownStyle = ComboBoxStyle.DropDownList;
            this.cbxReceiveEncoding.Text = System.Text.Encoding.Default.BodyName.ToUpper();

            this.cbxSendEncoding.Items.Clear();
            this.cbxSendEncoding.Items.Add(System.Text.Encoding.Default.BodyName.ToUpper());
            this.cbxSendEncoding.Items.Add(System.Text.Encoding.UTF8.BodyName.ToUpper());
            this.cbxSendEncoding.Items.Add(System.Text.Encoding.ASCII.BodyName.ToUpper());
            this.cbxSendEncoding.Items.Add("16进制");
            this.cbxSendEncoding.DropDownStyle = ComboBoxStyle.DropDownList;
            this.cbxSendEncoding.Text = System.Text.Encoding.Default.BodyName.ToUpper();

            this.txtResult.Dock = DockStyle.Fill;
        }

        private void MnuCheckUpdate_Click(object sender, EventArgs e)
        {
            //检查更新
            AppUpdaterBLL.CheckUpdate();
        }

        private void SysNotifyIcon_DoubleClick(object sender, EventArgs e)
        {
            this.Show();
            this.Activate();
        }

        private void MnuShow_Click(object sender, EventArgs e)
        {
            this.Show();
            this.Activate();
        }

        private void FrmMain_FormClosing(object sender, FormClosingEventArgs e)
        {
            this.sysNotifyIcon.Visible = false;
            this.sysNotifyIcon.Dispose();

            if (this._UDPSocket != null)
                this._UDPSocket.Dispose();

            if (this._TcpClient != null)
                this._TcpClient.Dispose();
            if (this._TcpServer != null)
                this._TcpServer.Dispose();
        }

        private void BtnSelect_Click(object sender, EventArgs e)
        {
            OpenFileDialog dialog = new OpenFileDialog();
            dialog.Filter = "文本文件|*.log;*.txt";
            if (dialog.ShowDialog() != DialogResult.OK)
                return;
            this.txtSendContent.Text = System.IO.File.ReadAllText(dialog.FileName);
        }

        private void BtnClear_Click(object sender, EventArgs e)
        {
            this.txtSendContent.Text = "";
        }

        private void MnuAbout_Click(object sender, EventArgs e)
        {
            new AboutBox().ShowDialog();
        }

        private void TbbSave_Click(object sender, EventArgs e)
        {
            SaveFileDialog dialog = new SaveFileDialog();
            dialog.Filter = "文本文件|*.log;*.txt";
            if (dialog.ShowDialog() != DialogResult.OK)
                return;
            System.IO.File.WriteAllText(dialog.FileName, this.txtResult.Text, Encoding.UTF8);
        }

        private void TbbClear_Click(object sender, EventArgs e)
        {
            this.txtResult.Text = "";
        }

        private void CbxSocketType_SelectedIndexChanged(object sender, EventArgs e)
        {
            if(this.cbxProtocolType.Text.ToLower() == "TCP Server".ToLower()
                || this.cbxProtocolType.Text.ToLower() == "UDP".ToLower())
            {
                this.labIPString.Text = "（2）本地IP地址";
                this.labPort.Text = "（3）本地端口号";
            }
            else
            {
                this.labIPString.Text = "（2）服务器IP地址";
                this.labPort.Text = "（3）服务器端口号";
            }
        }

        private void BtnConnect_Click(object sender, EventArgs e)
        {
            this.btnConnect.Enabled = false;
            this.Cursor = Cursors.WaitCursor;
            try
            {
                if (this.btnConnect.Text == "连接")
                {
                    if (this.txtIPString.Text.Trim() == "")
                    {
                        WinMessageBox.Show("IP地址不能为空！");
                        this.txtIPString.Focus();
                        return;
                    }
                    if (this.txtPort.Text.Trim() == "")
                    {
                        WinMessageBox.Show("端口号不能为空！");
                        this.txtPort.Focus();
                        return;
                    }
                    if (this.cbxProtocolType.Text.ToLower() == "UDP".ToLower())
                    {
                        this._UDPSocket = new UDPSocket(this.txtIPString.Text.Trim(), int.Parse(this.txtPort.Text));
                        //this._UDPSocket.ReceiveEncoding = System.Text.Encoding.GetEncoding(this.cbxReceiveEncoding.Text);
                        this._UDPSocket.ReceiveData += UDPSocket_ReceiveData;
                        if (this._UDPSocket.StartReceive() == false)
                        {
                            WinMessageBox.Show("连接服务器失败！");
                            this.sysStatusLabel2.Text = "连接服务器失败！";
                            return;
                        }
                        this.panelUDPSocket.Visible = true;
                    }
                    else if (this.cbxProtocolType.Text.ToLower() == "TCP Server".ToLower())
                    {
                        this._TcpServer = new TcpSocketServer();
                        //this._TcpServer.ReceiveEncoding = System.Text.Encoding.GetEncoding(this.cbxReceiveEncoding.Text);
                        this._TcpServer.RemoteConnect += TcpServer_RemoteConnect;
                        this._TcpServer.ReceiveData += TcpServer_ReceiveData;
                        this._TcpServer.RemoteClose += TcpServer_RemoteClose;
                        if (this._TcpServer.Start(this.txtIPString.Text.Trim(), int.Parse(this.txtPort.Text)) == false)
                        {
                            WinMessageBox.Show("连接服务器失败！");
                            this.sysStatusLabel2.Text = "连接服务器失败！";
                            return;
                        }

                        this.LoadConnect();
                        this.panelTcpServer.Visible = true;
                    }
                    else if(this.cbxProtocolType.Text.ToLower() == "TCP Client".ToLower())
                    {
                        this._TcpClient = new TcpSocketClient();
                        //this._TcpClient.ReceiveEncoding = System.Text.Encoding.GetEncoding(this.cbxReceiveEncoding.Text);
                        this._TcpClient.RemoteClose += TcpClient_RemoteClose;
                        if(this._TcpClient.Connect(this.txtIPString.Text.Trim(),int.Parse(this.txtPort.Text)) == false)
                        {
                            WinMessageBox.Show("连接服务器失败！");
                            this.sysStatusLabel2.Text = "连接服务器失败！";
                            return;
                        }

                        this._TcpClient.ReceiveData += TcpClient_ReceiveData;
                        if (this._TcpClient.StartReceive() == false)
                            return;

                        this.txtLocalIpString.Text = this._TcpClient.Socket.LocalEndPoint.ToString();
                        this.panelTcpClient.Visible = true;
                    }
                    this.txtResult.Text = "";
                    this.cbxProtocolType.Enabled = false;
                    //this.cbxProtocolType.ReadOnly = true;
                    this.cbxProtocolType.BackColor = System.Drawing.SystemColors.Info;
                    this.txtIPString.ReadOnly = true;
                    this.txtIPString.BackColor = System.Drawing.SystemColors.Info;
                    this.txtPort.ReadOnly = true;
                    this.txtPort.BackColor = System.Drawing.SystemColors.Info;

                    this.btnConnect.Text = "断开";
                }
                else
                {
                    if (this._UDPSocket != null)
                        this._UDPSocket.Close();
                    if (this._TcpClient != null)
                        this._TcpClient.Close();
                    if (this._TcpServer != null)
                        this._TcpServer.Close();

                    this.btnConnect.Text = "连接";
                    this.panelUDPSocket.Visible = false;
                    this.panelTcpServer.Visible = false;
                    this.panelTcpClient.Visible = false;
                    this.cbxProtocolType.Enabled = true;
                    this.cbxProtocolType.BackColor = System.Drawing.SystemColors.Window;
                    this.txtIPString.ReadOnly = false;
                    this.txtIPString.BackColor = System.Drawing.SystemColors.Window;
                    this.txtPort.ReadOnly = false;
                    this.txtPort.BackColor = System.Drawing.SystemColors.Window;
                }
            }
            finally
            {
                this.btnConnect.Enabled = true;
                this.Cursor = Cursors.Default;
            }
        }
        
        private void TcpServer_RemoteConnect()
        {
            if (this._TcpServer == null)
                return;
            this.LoadConnect();
        }

        private void LoadConnect()
        {
            this.cbxConntects.Invoke((EventHandler)delegate
            {
                this.cbxConntects.Items.Clear();
                this.cbxConntects.Items.Add("所有远程连接");
                foreach (var item in this._TcpServer.Sessions.Keys)
                {
                    this.cbxConntects.Items.Add(item);
                }
                this.cbxConntects.Text = "所有远程连接";
                this.cbxConntects.DropDownStyle = ComboBoxStyle.DropDownList;
            });
        }

        private void TcpServer_ReceiveData(IPEndPoint remote, byte[] buffer, int count)
        {
            this.AppendText(remote, buffer, count);
        }

        private void TcpServer_RemoteClose(string key)
        {
            if (this._TcpServer == null)
                return;

            this.LoadConnect();
        }
        
        
        private void TcpClient_ReceiveData(System.Net.IPEndPoint remote, byte[] buffer, int count)
        {
            this.AppendText(remote, buffer, count);
        }

        private void TcpClient_RemoteClose(string key)
        {
            this.btnConnect.PerformClick();
        }

        private void UDPSocket_ReceiveData(System.Net.IPEndPoint remote, byte[] buffer, int length)
        {
            this.AppendText(remote, buffer, length);
        }

        private void AppendText(System.Net.IPEndPoint remote, byte[] buffer, int length)
        {
            if (buffer == null || length <= 0 || buffer.Length < length)
                return;

            this.txtResult.Invoke((EventHandler)delegate
            {
                string data = string.Empty;
                if (this.cbxReceiveEncoding.Text == "16进制")
                    data = this.ByteToHex(buffer, 0, length);
                else
                    data = System.Text.Encoding.GetEncoding(this.cbxReceiveEncoding.Text).GetString(buffer, 0, length);
                if (this.ckbAppendAddress.Checked)
                    this.txtResult.AppendText("[" + remote.ToString() + "]");
                if (this.ckbAppendTime.Checked)
                    this.txtResult.AppendText("["+DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss fff")+"]");
                this.txtResult.AppendText(data);
                if (this.ckbAppendN.Checked)
                    this.txtResult.AppendText("\n");
                this.txtResult.ScrollToCaret();
            });
        }
        
        private void BtnSend_Click(object sender, EventArgs e)
        {
            this.btnSend.Enabled = false;
            this.Cursor = Cursors.WaitCursor;
            try
            {
                if(this.btnSend.Text == "发送")
                {
                    if (this.SendData() == false)
                        return;
                    this.sysStatusLabel2.Text = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss fff")+ "-发送成功";

                    //循环发送
                    if (this.cbxLoopSend.Checked)
                    {
                        int interval = 1000;
                        int.TryParse(this.txtLoopInterval.Text, out interval);
                        if (interval < 100)
                            interval = 100;

                        this._SysTimer = new System.Timers.Timer();
                        this._SysTimer.Interval = interval;
                        this._SysTimer.Elapsed += _SysTimer_Elapsed;
                        this._SysTimer.Start();

                        this.btnSend.Text = "停止发送";
                    }
                }
                else
                {
                    if (this._Worker != null && this._Worker.IsBusy)
                        this._Worker.CancelAsync();

                    this._SysTimer.Stop();
                    this.btnSend.Text = "发送";
                }
            }
            catch(Exception ex)
            {
                Logger.Error("发送数据异常：" + ex.ToString());
                MessageBox.Show("发送数据异常：" + ex.Message, "异常", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                this.btnSend.Enabled = true;
                this.Cursor = Cursors.Default;
            }
        }
        private System.Timers.Timer _SysTimer = null;
        private BackgroundWorker _Worker = null;

        private void _SysTimer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            if(this._Worker != null && this._Worker.IsBusy)
            {
                Logger.Info("发送线程还在运行!");
                this.sysStatusLabel2.Text = "发送线程还在运行!";
                return;
            }
            this._Worker = new BackgroundWorker();
            this._Worker.WorkerReportsProgress = true;
            this._Worker.WorkerSupportsCancellation = true;
            this._Worker.DoWork += _Worker_DoWork;
            this._Worker.RunWorkerAsync();
        }

        private void _Worker_DoWork(object sender, DoWorkEventArgs e)
        {
            this.SendData();
        }

        private bool SendData()
        {
            if (this.txtSendContent.Text.Trim() == "")
            {
                this.sysStatusLabel2.Text = "发送内容不能为空!";
                this.txtSendContent.Focus();
                return false;
            }
            byte[] bytes = new byte[0];
            if (this.cbxSendEncoding.Text == "16进制")
                bytes = this.HexToByte(this.txtSendContent.Text);
            else
                bytes = System.Text.Encoding.GetEncoding(this.cbxSendEncoding.Text).GetBytes(this.txtSendContent.Text);
            if (this.cbxProtocolType.Text.ToLower() == "UDP".ToLower())
            {
                if (this._UDPSocket == null || this._UDPSocket.Socket == null)
                {
                    WinMessageBox.Show("请先连接服务端！");
                    return false;
                }
                if (this.txtRemoteIPString.Text.Trim() == "")
                {
                    WinMessageBox.Warning("目标主机不能为空!");
                    this.txtRemoteIPString.Focus();
                    return false;
                }
                if (this.txtRemotePort.Text.Trim() == "")
                {
                    WinMessageBox.Warning("主机端口不能为空!");
                    this.txtRemotePort.Focus();
                    return false;
                }

                this._UDPSocket.SendTo(this.txtRemoteIPString.Text, int.Parse(this.txtRemotePort.Text), bytes);
            }
            else if (this.cbxProtocolType.Text.ToLower() == "TCP Server".ToLower())
            {
                if (this._TcpServer == null || this._TcpServer.Socket == null)
                {
                    WinMessageBox.Show("请先连接服务端！");
                    return false;
                }

                if (this._TcpServer.Sessions.Count <= 0)
                {
                    WinMessageBox.Warning("当前没有连接对象！");
                    return false;
                }
                if (this.cbxConntects.Text == "所有远程连接")
                {
                    this._TcpServer.SendTo(bytes);
                }
                else
                {
                    if (this._TcpServer.Sessions.ContainsKey(this.cbxConntects.Text) == false)
                    {
                        WinMessageBox.Show("当前没有连接对象！" + this.cbxConntects.Text);
                        return false;
                    }
                    var session = this._TcpServer.Sessions[this.cbxConntects.Text];
                    //this._TcpServer.SendTo(session.Client.RemoteEndPoint, bytes);//发送失败
                    this._TcpServer.SendTo(session, bytes);
                }
            }
            else if (this.cbxProtocolType.Text.ToLower() == "TCP Client".ToLower())
            {
                if (this._TcpClient == null || this._TcpClient.Socket == null)
                {
                    WinMessageBox.Show("请先连接服务端！");
                    return false;
                }

                this._TcpClient.Send(bytes);
            }
            return true;
        }

        /// <summary>
        /// 字节数组转16进制字符串
        /// </summary>
        /// <param name="bytes"></param>
        /// <param name="start"></param>
        /// <param name="count"></param>
        /// <returns></returns>
        public string ByteToHex(byte[] bytes, int start, int count)
        {
            StringBuilder result = new StringBuilder();
            try
            {
                if (bytes != null)
                {
                    for (int i = start; i < count; i++)
                    {
                        if (bytes.Length < i)
                            break;
                        //两个16进制用空格隔开,方便看数据
                        if (i == start)
                            result.Append(bytes[i].ToString("X2"));
                        else
                            result.Append(" " + bytes[i].ToString("X2"));
                    }
                }
                return result.ToString();
            }
            catch (Exception)
            {
                return "";
            }
        }

        /// <summary>
        /// 字符串转16进制格式,不够自动前面补零
        /// </summary>
        /// <param name="hexString"></param>
        /// <returns></returns>
        public byte[] HexToByte(string hexString)
        {
            hexString = hexString.Replace(" ", "");//清除空格
            if ((hexString.Length % 2) != 0)//奇数个
                hexString = hexString + " ";

            byte[] result = new byte[(hexString.Length) / 2];
            for (int i = 0; i < result.Length; i++)
            {
                result[i] = Convert.ToByte(hexString.Substring(i * 2, 2).Replace(" ", ""), 16);
            }
            return result;
        }
    }
}
